[Theory Talks](http://www.theory-talks.org/) by Peer Schouten is licensed under the [Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License](https://creativecommons.org/licenses/by-nc-nd/3.0/).

![Website](https://img.shields.io/website?url=http%3A%2F%2Fwww.theory-talks.org)